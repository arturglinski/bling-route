import { Component, HostListener, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { trigger, animate, style, group, query, transition, state } from '@angular/animations';
import { routerAnimation } from './router.animation';
import { AppRouting } from './app.routing';
import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [routerAnimation]
})
export class AppComponent implements OnInit {
  public currentView = 1;

  constructor(private activatedRoute: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    // console.log('this.router', this.router);

    console.log('currentView', this.currentView);
  }

  prepRouteState(outlet: any) {
    return outlet.activatedRouteData.state || 'home';
  }

  scrollDown() {
    if (this.currentView < this.router.config.length - 2) {
      this.router.navigate([this.router.config[++this.currentView].path]);
      console.log('currentView', this.currentView);
    } else {
      this.currentView = 1;
      this.router.navigate([this.router.config[this.currentView].path]);
      console.log('currentView', this.currentView);
    }
  }

  scrollUp() {
    if (this.currentView > 1) {
      this.router.navigate([this.router.config[--this.currentView].path]);
      console.log('currentView', this.currentView);
    } else {
      console.log('currentView', this.currentView);
      this.currentView = this.router.config.length - 2;
      this.router.navigate([this.router.config[this.currentView].path]);
    }
  }

  @HostListener('wheel', ['$event']) onWindowScroll($event) {
    console.log('wheelDelta', $event.wheelDelta);
    // no 1st no last

    if ($event.wheelDelta < 0) {
      this.scrollDown()
    } else {
      this.scrollUp();
    }
  }
}
