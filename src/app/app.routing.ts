import { Routes, RouterModule } from '@angular/router';
import { LocationStrategy, PathLocationStrategy } from '@angular/common';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { InfoComponent } from './info/info.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent, data: { state: 'home' } },
  { path: 'about', component: AboutComponent, data: { state: 'about' } },
  { path: 'info', component: InfoComponent, data: { state: 'info' } },
  { path: '**', redirectTo: 'home', pathMatch: 'full' } // powinno byc 404
];

// export const AppRouting = RouterModule.forRoot(routes, { enableTracing: true });
export const AppRouting = RouterModule.forRoot(routes);
