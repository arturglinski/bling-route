import { trigger, animate, style, group, query, transition } from '@angular/animations';

export const routerAnimation = trigger('routerAnimation', [
  // transition('* <=> *', [
  //   query(':enter, :leave', style({ position: 'fixed', width: '100%' })
  //     , { optional: true }),
  //   group([
  //     query(':enter', [
  //       style({ transform: 'translateY(100%)' }),
  //       animate('0.75s ease-in-out', style({ transform: 'translateY(0%)' }))
  //     ], { optional: true }),
  //     query(':leave', [
  //       style({ transform: 'translateY(0%)' }),
  //       animate('0.75s ease-in-out', style({ transform: 'translateY(-100%)' }))
  //     ], { optional: true })
  //   ])
  // ])

  transition('home => about', [
    query(':enter, :leave', style({ position: 'fixed', width: '100%' })
      , { optional: true }),
    group([
      query(':enter', [
        style({ transform: 'translateY(100%)' }),
        animate('0.75s ease-in-out', style({ transform: 'translateY(0%)' }))
      ], { optional: true }),
      query(':leave', [
        style({ transform: 'translateY(0%)' }),
        animate('0.75s ease-in-out', style({ transform: 'translateY(-100%)' }))
      ], { optional: true })
    ])
  ]),

  transition('about => info', [
    query(':enter, :leave', style({ position: 'fixed', width: '100%' })
      , { optional: true }),
    group([
      query(':enter', [
        style({ transform: 'translateY(100%)' }),
        animate('0.75s ease-in-out', style({ transform: 'translateY(0%)' }))
      ], { optional: true }),
      query(':leave', [
        style({ transform: 'translateY(0%)' }),
        animate('0.75s ease-in-out', style({ transform: 'translateY(-100%)' }))
      ], { optional: true })
    ])
  ]),

  transition('info => home', [
    query(':enter, :leave', style({ position: 'fixed', width: '100%' })
      , { optional: true }),
    group([
      query(':enter', [
        style({ transform: 'translateY(100%)' }),
        animate('0.75s ease-in-out', style({ transform: 'translateY(0%)' }))
      ], { optional: true }),
      query(':leave', [
        style({ transform: 'translateY(0%)' }),
        animate('0.75s ease-in-out', style({ transform: 'translateY(-100%)' }))
      ], { optional: true })
    ])
  ]),

  transition('about => home', [
    query(':enter, :leave', style({ position: 'fixed', width: '100%' })
      , { optional: true }),
    group([
      query(':enter', [
        style({ transform: 'translateY(0%)' }),
        animate('0.75s ease-in-out', style({ transform: 'translateY(100%)' }))
      ], { optional: true }),
      query(':leave', [
        style({ transform: 'translateY(-100%)' }),
        animate('0.75s ease-in-out', style({ transform: 'translateY(0%)' }))
      ], { optional: true })
    ])
  ]),

  transition('info => about', [
    query(':enter, :leave', style({ position: 'fixed', width: '100%' })
      , { optional: true }),
    group([
      query(':enter', [
        style({ transform: 'translateY(0%)' }),
        animate('0.75s ease-in-out', style({ transform: 'translateY(100%)' }))
      ], { optional: true }),
      query(':leave', [
        style({ transform: 'translateY(-100%)' }),
        animate('0.75s ease-in-out', style({ transform: 'translateY(0%)' }))
      ], { optional: true })
    ])
  ]),

  transition('home => info', [
    query(':enter, :leave', style({ position: 'fixed', width: '100%' })
      , { optional: true }),
    group([
      query(':enter', [
        style({ transform: 'translateY(0%)' }),
        animate('0.75s ease-in-out', style({ transform: 'translateY(100%)' }))
      ], { optional: true }),
      query(':leave', [
        style({ transform: 'translateY(-100%)' }),
        animate('0.75s ease-in-out', style({ transform: 'translateY(0%)' }))
      ], { optional: true })
    ])
  ]),






])

