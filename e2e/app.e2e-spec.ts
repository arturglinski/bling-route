import { RouteAnimationPage } from './app.po';

describe('route-animation App', () => {
  let page: RouteAnimationPage;

  beforeEach(() => {
    page = new RouteAnimationPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
